import React, { useState } from 'react';

function ShoesList(props) {
  const [successMessage, setSuccessMessage] = useState(false);

  const shoeDelete = async (shoe) => {
    const deleted = shoe.id;
    const url = `http://localhost:8080/api/shoes/${deleted}/`;
    const fetchConfig = {
        method: "delete",
        headers: {
            'Content-Type': 'application/json',
        },
    };

      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setSuccessMessage(true);
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);
          window.location.reload(false);
      }
    }

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model }</td>
                <td>{ shoe.color }</td>
                <td>
                    <img src={shoe.picture} alt="" width="100px" height="100px"/>
                </td>
                <td>{ shoe.bin }</td>
                <td>
                  <button className="btn btn-outline-danger" onClick={() => shoeDelete(shoe)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
        {successMessage && (
              <div className="alert alert-danger mt-3" role="alert">
                Shoe successfully deleted!
              </div>
            )}
      </table>
    );
  }
  
  export default ShoesList;