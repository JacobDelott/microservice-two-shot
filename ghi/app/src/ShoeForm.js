import React, { useState, useEffect } from 'react';

function ShoeForm() {
  const [bins, setBins] = useState([]);
  const [formData, setFormData] = useState({
    manufacturer: '',
    model: '',
    color: '',
    picture: '',
    bin: ''
  });
  const [successMessage, setSuccessMessage] = useState(false);

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/bins/');
      if (response.ok) {
        const data = await response.json();
        setBins(data.bins);
      }

    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const shoesUrl = "http://localhost:8080/api/shoes/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const shoeResponse = await fetch(shoesUrl, fetchConfig);

    if (shoeResponse.ok) {
      setFormData({
        manufacturer: '',
        model: '',
        color: '',
        picture: '',
        bin: ''
      });
      setSuccessMessage(true);
      setTimeout(() => {
        setSuccessMessage(false);
      }, 3000);
    }
  }

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value
    }));
  };

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              
              <div className="form-floating mb-3">
                <input
                  placeholder="Manufacturer"
                  required
                  type="text"
                  name="manufacturer"
                  id="manufacturer"
                  className="form-control"
                  value={formData.manufacturer}
                  onChange={handleChange}
                />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>

              <div className="form-floating mb-3">
                <input
                  placeholder="Model"
                  required
                  type="text"
                  name="model"
                  id="model"
                  className="form-control"
                  value={formData.model}
                  onChange={handleChange}
                />
                <label htmlFor="model">Model</label>
              </div>
              
              <div className="form-floating mb-3">
                <input
                  placeholder="Color"
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                  value={formData.color}
                  onChange={handleChange}
                />
                <label htmlFor="color">Color</label>
              </div>
              
              <div className="form-floating mb-3">
                <input
                  placeholder="Picture"
                  required
                  type="url"
                  name="picture"
                  id="picture"
                  className="form-control"
                  value={formData.picture}
                  onChange={handleChange}
                />
                <label htmlFor="picture">Picture</label>
              </div>
              
              <div className="mb-3">
                <select
                  required
                  name="bin"
                  id="bin"
                  className="form-select"
                  value={formData.bin}
                  onChange={handleChange}
                >
                  <option value="">Choose a bin</option>
                  {bins.map((bin) => {
                    return (
                      <option key={bin.href} value={bin.href}>
                        {bin.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
            
              <button className="btn btn-primary" type="submit">
                Create
              </button>
            
            </form>
            {successMessage && (
              <div className="alert alert-success mt-3" role="alert">
                Shoe successfully created!
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
