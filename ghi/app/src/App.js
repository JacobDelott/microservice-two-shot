import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatsList from "./HatsList";
import { useEffect, useState } from "react";
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import CreateHatForm from "./HatForm";

function App(props) {
  if (props.shoes === undefined || props.hats === undefined) {
    return null;
  }
  /* const [hats, setHats] = useState([]);

  async function getHats() {
    const url = "http://localhost:8090/api/hats/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }
  useEffect(() => {
    getHats();
  }, []); */
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsList hats={props.hats} />} />
          <Route path="/shoes" element={<ShoesList shoes={props.shoes} />} />
          <Route path="/create-shoes" element={<ShoeForm /> } />
          <Route path="/createHatForm/" element={<CreateHatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
