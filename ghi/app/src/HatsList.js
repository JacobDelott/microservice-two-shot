function HatsList(props) {
  const deleteHat = async (hat) => {
    try {
      const deleted = hat.id;
      const hatsUrl = `http://localhost:8090/api/hats/${deleted}/`;
      const fetchConfig = {
        method: "delete",
      };
      const response = await fetch(hatsUrl, fetchConfig);
      if (response.ok) {
        console.log("Hat deleted");
        window.location.reload(false);
      }
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Fabric</th>
          <th>Style Name</th>
          <th>Color</th>
          <th>Image url</th>
          <th>Location</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {props.hats.map((hat) => {
          return (
            <tr key={hat.id}>
              <td>{hat.fabric}</td>
              <td>{hat.style_name}</td>
              <td>{hat.color}</td>
              <td>
                <img width={150} src={hat.image_url} />
              </td>
              <td>{hat.location}</td>
              <td>
                <button onClick={() => deleteHat(hat)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList;
