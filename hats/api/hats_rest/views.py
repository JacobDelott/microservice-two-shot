from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

class LocationVODetailEndoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href"]

class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = ["id","fabric","style_name","color","image_url"]


    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "image_url",
        "location",
     ]
    encoders = {
        "location":LocationVODetailEndoder(),
    }
# Create your views here.


@require_http_methods(["GET","POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
            safe=False

        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )


        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

# @require_http_methods(["DELETE", "GET", "PUT"])
# def api_show_hat(request, pk):
#     if  request.method == "GET":
#         try: 
#             hat = Hat.objects.get(id=pk)
#             return JsonResponse(
#                 hat,
#                 encoder=HatDetailEncoder,
#                 safe=False
#             )
#         except Hat.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response
#     elif request.method == "DELETE":
#         try:
#             hat = Hat.objects.get(id=pk)
#             hat.delete()
#             return JsonResponse(
#                 hat,
#                 encoder=HatDetailEncoder,
#                 safe=False,
#             )
#         except Hat.DoesNotExist:
#             return JsonResponse({"message": "Does not exist"})
#     else: # PUT
#         try:
#             content = json.loads(request.body)
#             hat = Hat.objects.get(id=pk)

#             props = []
#             for prop in props:
#                 if prop in content:
#                     setattr(location, prop, content[prop])
#             location.save()
#             return JsonResponse(
#                 location,
#                 encoder=LocationEncoder,
#                 safe=False,
#             )
#         except Location.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response
@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
