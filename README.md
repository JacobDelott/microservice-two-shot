# Wardrobify

Team:

- Jacob Delott - Hats
- Julius Pagtakhan - Shoes

## Design

Wardrobify is an app that allows users to organize their hats and shoes.

## Shoes microservice

Shoes microservice will allow the user to list, create,
and delete shoes based upon their corresponsing bins value objects.

The user can create shoe objects with property details- manufacturer,
model name, color, picture URL, and the corresponding bin id.

RESTful APIs allow the user to view, create, and delete shoe objects.

Polling will refresh inventory data from the wardrobe microservice.

## Hats microservice

Hats microservice will allow the user to list, create,
and delete hats based upon their corresponsing location value objects.
The user can create hat objects with property details- style_name,
fabric, color, picture URL, and the corresponding location id.
RESTful APIs allow the user to view, create, and delete hat objects.
Polling will refresh inventory data from the wardrobe microservice.
